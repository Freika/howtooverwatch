activate :directory_indexes
set :relative_links, true

configure :build do
  activate :minify_css # Minify CSS on build
  activate :minify_javascript # Minify Javascript on build
  set :build_dir, 'public' # set the build directory to GitLab Pages 'public' folder
  activate :relative_assets # Use relative URLs
end

activate :meta_tags
